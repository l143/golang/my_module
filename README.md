# Ejemplo de Módulo en Golang

Los módulos permiten poder crear proyectos fuera del GO_PATH.

Para poder crea un módulo se debe usar la instrucción **go mod init** conbinandolo con el repositorio y nombre de usuario. 

```
$ go mod init github.com/everitosan/my_module
```

Esto creará un archivo go.mod que indicará que esto es un paquete.